const expect = require('chai').expect;
const documentSearch = require('../src/document-search');

const productionDocumentTree = require("../src/document-tree.json");


describe('Document Search', function () {

  describe('Basic Document Search Term Tests', function () {

    it('searching for the first child should find the node', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: 'child',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'parent');

      expect(foundNodes).to.have.lengthOf(1);
      expect(foundNodes[0].label).to.equal('parent');
    });


    it('searching for a child of a child should find the node', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: 'child',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'child');

      expect(foundNodes).to.have.lengthOf(1);
      expect(foundNodes[0].label).to.equal('child');
    });

    it('search term should be case insensitive', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: 'child',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'chILd');
      expect(foundNodes).to.have.lengthOf(1);
      expect(foundNodes[0].label).to.equal('child');
    });

    it('search term and label should be case insensitive and return the original label formatting', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: 'Child',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'chIlD');
      expect(foundNodes).to.have.lengthOf(1);
      expect(foundNodes[0].label).to.equal('Child');
    });

    it('when finding nodes document search should return the original node object properties + structure', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: 'child',
              children: [],
              href: 'http://myhref.com',
              id: 'myId',
              type: 'myType',
              descendants: {},
              ancestors: {},
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'child');
      expect(foundNodes).to.have.lengthOf(1);

      expect(foundNodes[0].label).to.equal('child');
      expect(foundNodes[0].children).to.be.empty;
      expect(foundNodes[0].href).to.equal('http://myhref.com');
      expect(foundNodes[0].id).to.equal('myId');
      expect(foundNodes[0].type).to.equal('myType');
      expect(foundNodes[0].label).to.equal('child');
      expect(foundNodes[0].descendants).to.exist;
      expect(foundNodes[0].ancestors).to.exist;
    });

    it('when searching with spaces the space should be treated like a wildcard', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: '(Q-1) Notary Information',
              children: []
            }, {
              label: 'Shouldnt Match',
              children: []
            }]
          }]
        }
      }

      const firstFoundNodes = documentSearch(document, 'No fo');
      expect(firstFoundNodes).to.have.lengthOf(1);

      const secondFoundNodes = documentSearch(document, 'No fo on');
      expect(secondFoundNodes).to.have.lengthOf(1);
    });

    it('when searching with parens () they should not be treated as regex', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: '(Q-1) Notary Information',
              children: []
            }, {
              label: 'Shouldnt Match',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, '(Q-');
      expect(foundNodes).to.have.lengthOf(1);
    });

    it('when nodes have no label, document search should not blow up', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              label: '(Q-1) Notary Information',
              children: []
            }, {
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'Notary');
      expect(foundNodes).to.have.lengthOf(1);
    });


  });

  describe('Basic Document Search Sorting Tests', function () {
    // Define: "Nearly Exact" means minus grammar, parens, dashes, etc
    // Define: "Exact Matches" is finding the exact term, with more weight if there is empty space before+after the terms

    // -- term/spelling matches --
    // Full Matches
    // Exact Matches at Beginning
    // Exact Matches Within
    // Nearly Exact Full Match
    // Nearly Exact Matches at Beginning
    // Nearly Exact Matches Within
    // Partial Match at Beginning
    // Partial Match
    // Mispellings/Trigram Match

    // --  Region & Header is searched more often --
    // extra weight on those?  Multiplier? Constant add?

    it('document search should return headers above non headers', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              id: 'a',
              label: 'title',
              type: 'table',
              children: []
            }, {
              id: 'b',
              label: 'title',
              type: 'header',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'title');
      expect(foundNodes).to.have.lengthOf(2);
      expect(foundNodes[0].id).to.equal('b');
      expect(foundNodes[1].id).to.equal('a');
    });

    it('document search should return regions above non regions', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              id: 'a',
              label: 'title',
              type: 'region',
              children: []
            }, {
              id: 'b',
              label: 'title',
              type: 'table',
              children: []
            },{
              id: 'c',
              label: 'title',
              type: 'region',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'title');
      expect(foundNodes).to.have.lengthOf(3);
      expect(foundNodes[0].id).to.equal('a');
      expect(foundNodes[1].id).to.equal('c');
      expect(foundNodes[2].id).to.equal('b');
    })

    it('document search should sort fuller matches to the top', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              id: 'a',
              label: 'something else with cats are awesome in it',
              children: []
            }, {
              id: 'b',
              label: 'Something with cats',
              children: []
            },{
              id: 'c',
              label: 'cats are awesome',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'cats are awesome');
      expect(foundNodes).to.have.lengthOf(2);
      expect(foundNodes[0].id).to.equal('c');
      expect(foundNodes[1].id).to.equal('a');
    })

    it('document search should sort fuller matches higher than wildcard matches', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              id: 'a',
              label: 'A-2 Cat and Dog Scan',
              children: []
            }, {
              id: 'b',
              label: 'A-1 Cat Scan',
              children: []
            },{
              id: 'c',
              label: 'B-3 Dogscan',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'cat scan');
      expect(foundNodes).to.have.lengthOf(2);
      expect(foundNodes[0].id).to.equal('b');
      expect(foundNodes[1].id).to.equal('a');
    })

    it('document search should sort closer wildcard matches higher than further apart wildcard matches', function () {
      const document = {
        root: {
          children: [{
            label: 'parent',
            children: [{
              id: 'a',
              label: 'A-2 Cat and Dog Scan',
              children: []
            }, {
              id: 'b',
              label: 'A-1 Cat Scan',
              children: []
            },{
              id: 'c',
              label: 'B-3 Cat be scanned',
              children: []
            }]
          }]
        }
      }

      const foundNodes = documentSearch(document, 'cat scan');
      expect(foundNodes).to.have.lengthOf(3);
      expect(foundNodes[0].id).to.equal('b');
      expect(foundNodes[1].id).to.equal('c');
      expect(foundNodes[2].id).to.equal('a');
    })

  });

  describe('Production Document Search Tests', function () {

    it('searching `pract` should find the two matching document', function () {
      const foundNodes = documentSearch(productionDocumentTree, 'pract');

      expect(foundNodes).to.have.lengthOf(2);

      expect(foundNodes[0].label).to.equal('Accounting Practices');
      expect(foundNodes[0].type).to.equal('header');
      expect(foundNodes[0].href).to.equal('/filings/A12Beef234Mayo241Love/document/nodes/Header_01-A');

      expect(foundNodes[1].label).to.equal('Not Practicable to Estimate Fair Value');
      expect(foundNodes[1].type).to.equal('header');
      expect(foundNodes[1].href).to.equal('/filings/A12Beef234Mayo241Love/document/nodes/Header_20-D');
    });

    it('searching `Collateral for Derivative Instruments` should find the one matching document', function () {
      const foundNodes = documentSearch(productionDocumentTree, 'Collateral for Derivative Instruments');

      expect(foundNodes).to.have.lengthOf(1);
      expect(foundNodes[0].label).to.equal('(E-09) Schedule DB - Part D - Section 2 - Collateral for Derivative Instruments Open as of Current Statement Date');
      expect(foundNodes[0].type).to.equal('region');
      expect(foundNodes[0].href).to.equal('/filings/A12Beef234Mayo241Love/document/nodes/Region_E_009_E09_SCDB-PTD-SN2_00000197');
    });

    it('searching for "Q 1" it should include (Q-1) in the results', function () {
      const foundNodes = documentSearch(productionDocumentTree, '(Q 1)');

      let foundQ1Example = false;
      for (let currentNode of foundNodes) {
        if (currentNode.label.includes('(Q-1)')) {
          foundQ1Example = true;
          break;
        }
      }

      expect(foundQ1Example, 'failed to find a node with "(Q-1)"').to.be.true;
    });

    it('searching for "assets in" should sort a closer matched, non region/header toward the top', function () {
      const foundNodes = documentSearch(productionDocumentTree, 'assets in');

      expect(foundNodes[0].label).to.equal('Restricted assets (including pledged)');
      expect(foundNodes[1].label).to.equal('(Q-2) Assets - Inline');
    });

  });

});
