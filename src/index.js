const express = require("express");
const documentTree = require("./document-tree.json");
const documentSearch = require('./document-search');
const app = express();
const port = 8080;


app.get("/quickNav/:query", (req, res) => {
  const {query} = req.params;

  // ⚠️ HERE IS WHERE YOU'LL NEED TO ADD CODE
  // ⚠️ TO GET THIS WORKING ❤️

  const foundDocuments = documentSearch(documentTree, query)

  if (foundDocuments.length >= 1) {
    const simplifiedDocuments = foundDocuments.map(currentDocument => {
      return {
        label: currentDocument.label,
        url: currentDocument.href,
        type: currentDocument.type
      }
    });

    res.send(simplifiedDocuments);
  } else {
    res.sendStatus(404);
  }

});

// This serves the HTML interface
app.use(express.static("public"));

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});
