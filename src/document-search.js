const fuzzysort = require('fuzzysort');

const SortStrategies = { BASIC: 'BASIC', CUSTOM_COMPLEX: 'CUSTOM_COMPLEX', THIRD_PARTY: 'THIRD_PARTY' };


const SORT_STRATEGY = SortStrategies.CUSTOM_COMPLEX;
const PERCENTAGE_LENGTH_MATCH_MULTIPLIER = 15;
const WILDCARD_CLOSE_MATCH_MULTIPLIER = 10;
const TYPE_WEIGHT_ADDITION = 1;

function documentSearch(documentTree, labelSearchTerm) {
  const foundNodes = depthFirstSearch(documentTree, labelSearchTerm);
  const weightedFoundNodes = addMatchStrengthToNodesForSearchTerm(foundNodes, labelSearchTerm);
  const foundSortedNodes = sortNodesByMatchStrength(weightedFoundNodes, labelSearchTerm);

  // should maybe delete weights after to put nodes back how we found it.  or should have wrapped the objects all together to add a weight to leave originals untouched.
  return foundSortedNodes;
}

function depthFirstSearch(documentTree, labelSearchTerm) {
  return recursiveDepthFirstSearch(documentTree.root.children, labelSearchTerm);
}

function recursiveDepthFirstSearch(currentNodeList, labelSearchTerm) {
  let matchingNodes = [];

  for (const currentNode of currentNodeList) {

    if (currentNode.label && labelIncludesSearchTerm(currentNode.label, labelSearchTerm)) {
      matchingNodes.push(currentNode);
    }

    if(currentNode.children.length >= 1) {
      matchingNodes = matchingNodes.concat(recursiveDepthFirstSearch(currentNode.children, labelSearchTerm));
    }
  }

  return matchingNodes;
}

function labelIncludesSearchTerm(label, searchTerm) {
  let modifiedSearchTerm = searchTerm.toLowerCase();
  modifiedSearchTerm = escapeRegExp(modifiedSearchTerm);
  modifiedSearchTerm = modifiedSearchTerm.replace(/\s/g, '.*');
  const searchRegex = new RegExp(modifiedSearchTerm);

  let modifiedLabel = label.toLowerCase();
  const regexFound = searchRegex.test(modifiedLabel);

  return regexFound;
}

// could keep track of how deep each one was found in case that's helpful to order by later
// could keep track if search term was found first, or as a whole word
// could start putting all these "coulds" into a score to return on the node, or a wrapper object to the node, to inform sorting suggestions later

/**
 * if not matched at all, return 0
 * if matched, a weight between 1-infinity will be given for how strong the match was
 */
function calculateMatchStrengthForLabelAndSearchTerm(label, searchTerm) {
  let matchStrength = 0;

  let modifiedSearchTerm = searchTerm.toLowerCase();
  let modifiedLabel = label.toLowerCase();

  // Calculate match strength based on ~"percentage" length of match
  let shrunkenLabelIfFound = modifiedLabel.replace(modifiedSearchTerm, '');
  if (shrunkenLabelIfFound.length < modifiedLabel.length) {
    // more weight the more characters that match.
    // if label is 10 chars long, and shrunken label is 0 (meaning exact match) then 10 / (0+1) = 10
    // if label is 10 chars long, and shrunken label is 9 (matched 1 char) then 10 / (9+1) = 1
    matchStrength = (modifiedLabel.length) / (shrunkenLabelIfFound.length + 1)  // +1 so we don't divide by 0 when they match exactly
    matchStrength *= PERCENTAGE_LENGTH_MATCH_MULTIPLIER;
    return matchStrength;
  }


  // Calculate match strength on how close a wildcard matches
  let wildcardSearchTerm = escapeRegExp(modifiedSearchTerm);
  wildcardSearchTerm = wildcardSearchTerm.replace(/\s/g, '.*');
  const wildcardSearchRegex = new RegExp(wildcardSearchTerm);
  const shrunkenLabelIfWildcardMatched = modifiedLabel.replace(wildcardSearchRegex, '');
  if (shrunkenLabelIfWildcardMatched.length < modifiedLabel.length) {
    // a length closest to label.length - search.length was a closer match, than a smaller length which means it was a long stretched match
    // equation = (shrunken length) / (perfect match length)   then multiplied by (search term length) / (label length)
    //   this last multiplication is to help normalize the benefit long labels get with this equation compared to short labels.

    // Example Label: Cat Swings, Search Term: "c t"
      // label is 10 chars long, shrunken is 7 and search term is 3 (meaning a close match) then     7 / (10 - 3) = 1     * (3/10) = .3
    // Example Label: Cat Swings, Search Term: "c g"
      // label is 10 chars long, shrunken is 1 and search term is 3 (meaning a poor/far match) then  1 / (10 - 3) = .143  * (3/10) = .043
    // Example Label: Cat Swings, Search Term: "cat wi"
      // label is 10 chars long, shrunken is 3 and search term is 6 (meaning a close match) then     3 / (10 - 6) = .75   * (6/10) = .45
    matchStrength = shrunkenLabelIfWildcardMatched.length / (modifiedLabel.length - modifiedSearchTerm.length);
    matchStrength *= modifiedSearchTerm.length / modifiedLabel.length;
    matchStrength *= WILDCARD_CLOSE_MATCH_MULTIPLIER;
    return matchStrength
  }

  return 0;
}

// found on https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function addMatchStrengthToNodesForSearchTerm(foundNodes, labelSearchTerm) {
  for (let currentNode of foundNodes) {
    currentNode.matchStrength = 1;

    if (SORT_STRATEGY === SortStrategies.CUSTOM_COMPLEX) {
      const complexMatchStrength = calculateMatchStrengthForLabelAndSearchTerm(currentNode.label, labelSearchTerm);
      currentNode.matchStrength += complexMatchStrength;
    }

    if (currentNode.type === 'header' || currentNode.type === 'region') {
      currentNode.matchStrength += TYPE_WEIGHT_ADDITION;
    }

  }

  return foundNodes;
}

function sortNodesByMatchStrength(foundNodes, labelSearchTerm) {
  let sortedNodes = [].concat(foundNodes);

  if (SORT_STRATEGY === SortStrategies.THIRD_PARTY) {

    // https://github.com/farzher/fuzzysort
    const fuzzySortResults = fuzzysort.go(labelSearchTerm, sortedNodes, {key: 'label'})
    sortedNodes = fuzzySortResults.map((currentNode) => currentNode.obj);
    // TODO: need to add in the already calculated weights, like header/region weights. Need to figure that out in this library. There's a way, but it confused me. So I abandoned ship.

  } else {

    sortedNodes = sortedNodes.sort((a, b) => {return b.matchStrength - a.matchStrength });
  }

  return sortedNodes;
}

module.exports = documentSearch;