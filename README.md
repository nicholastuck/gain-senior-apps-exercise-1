# Welcome to the Gain Compliance Senior Engineering Exercise

## Welcome!

See the bottom of this document for an overview of our technical interview flow. If you need further information on any of this please don't hesitate to email me: brian@gaincompliance.com. This is the first time we've used this exercise, so if you are confused, it's probably on us.

## The Exercise!

Gain Compliance's customers currently navigate their document through a tree and menu system. And because each document has multiple sections with dozens of subsections – it can take a lot of time to hop around to complete their work.

We've been prototyping some new "Quick Navigation" designs, to allow for navigation between sections. We'd like to share these with you, and have you help us complete the implementation.

The idea is to present an input field for the customer to enter a phrase in, and have matching document sections show up below. Thankfully our application team has already built the user -interface parts, they just need your help to finish the implementation. (That said, if you want to make changes to the presentation layer, you are more than welcome! In fact everything about this exercise can be changed, if you want.)

Okay so let's get down to the part you get to build:

1. **Get Familiar with:** the Document Outline – The document structure we'll be using for this example is located in the static file `src/document-tree.json` please be familiar with this. Take note that the entire structure is based on _nodes_ and _children._

2. **Explore:** the basic [Express.js](https://expressjs.com) node server – This is located in a single file `src/index.js`.

3. **Note:** We've loaded the tree and setup a stub GET handler for you at `/quickNav/:query`. You should see that the current interface is requesting this end point when the search string is modified.

4. **Add:** The ability to search the document's tree structure atleast using the `label` property.

5. **Add:** Respond with a flat list of tree nodes that matches the query. A few things to keep in mind, the interface is currently expecting a list of JSON objects with the properties: `label`, `url` and `type` to be present. You can send more of the node back if you'd like.

6. **Keep in mind:** If no matches are found, return a 404.

7. **Keep in mind:** Search should be case insensative.

8. **Keep in mind:** How white space and word fragments can be used to quickly find matching sections

9. **Keep in mind:** Search results should be ordered intentionally. There are a bunch of strategies to use, and we'll leave that up you. The product team has said that they tend to freqently navigate between these types most: region & header.

10. **Explore:** Could search results be impacted by how often clients select different sections?

## Example Searches

Our product team has quickly put together a couple of quick reference searches to explain how they'd like this to work.

Imagine this simple list of document tree nodes `label` properties:

- (Q-1) Company Name
- (Q-1) Notary Information
- Worksheet for Cash Flow, Line 2, Net Investment Income
- Accounting Practices

The example phrases "q-1" and "Q 1" should match the following labels, and return their nodes:

- (Q-1) Company Name
- (Q-1) Notary Information

The example phrase "pract" should match nodes with the label:

- Accounting Practices

The example phrase "No fo" should match nodes for the label:

- (Q-1) Notary Information

## About this Process

Hello and welcome to Gain Compliance’s technical interview process. We are excited to see your work and talk with you. Interviewing, especially the technical side, can be a stressful process, and we’ve designed an opportunity for you to demonstrate your abilities in a low stress environment. This means you get to do some work at home, and then join us on a call to talk through it. No live coding.

In the coming days we are going to schedule a 60 minute call with you. Be prepared to share your screen on Google Meet. This is not a pass or fail test. While we want to see how you tackle these problems, we are really interested in how you think about them, how you communicate and how comfortable you are analyzing your work. You don't need to send us any results before this, sharing your work on the screen, on a Google Meet with us is perfect.

### Expected Questions

We'll likely ask you the following questions on the call, no tricks and no surprises:

- What was the first thing you did when you received this exercise?
  - Read the email. Super skimmed the readme. 
  - Typed in the search box. Nothing happened.
  - Poked around all the files blindly.
  - Skimmed the readme.
  - Created an account. Forked it. Downloaded it. Added git. Opened in IntelliJ.
  - Figure out why it wouldn't run.  Port 8080 already in use.
  - More diligently read the readme.  
  - Hard coded a response from the server to see if/how the UI worked. Tried just the node to see what would happen. Then realized I forgot to turn it into an array when hardcoding one :|
  - Poked around the UI code to see the basics there. Play with the "color" param that didn't do anything :)
  - Figured out how to add testing / mocha + chai. Move to a separate file and setup the test.
  - Add in the mapping from document to ui object.
  - Start writing tests and hacking the search algorithm over and over. TDD style.
- Why did you choose this approach?
  - ? Which part ? :) 
- Were there interesting optimizations you discovered?
  - There are stronger matching criteria that once you find, you can skip the other matching criteria.
  - I found the speed of the brute force depth first search to be much faster than I expected.  14k lines isn't a crazy huge document but seems pretty instant.
  - I was stuck between searching/filtering the list, then calculating the weights, vs calculating while traversing the tree. Performance vs maintainability trade off.
  - Could run the document through multiple searches, prioritizing the best matches, then if none found, send it back through for more.
- What was the most difficult part of this?
  - At first I was attempting to deal with the full document tree for the cases, this was silly and made failures nearly impossible to debug.
  - There's like 20 ways to do everything, so deciding on which one to try, then which one to stick with was what I struggled with the most.
  - Sorting broke my fragile tests that I knew would bite me in the butt.
- What did you do when you got stuck?
  - Made the tests simplier/smaller so I can debug + step through the code to see what was going on. 
- If you had another few days on this project what would you do?
  - The product ideas below.
- What would you need to explain to your teammates so they could offer you a good review?
  - The Biggest one to cover: Go over how the complex weighting works in depth, maybe even running examples and adding tests to help understand and answer questions  
  - However, I like to give a brief overview for a code review on almost everything, not a line by line, but chunks of code to explain.
  - Here I think showing the UI is working. 
  - Show what the object tree looks like, and which parts we care about in this specific.
  - Highlight how the recursion works.
  - Highlight how the response objects will have all their children still, but those contents will be ignored in this process at this point  
  - Highlight the trickyness with the regex (wildcard regex, but ignoring what appears like regex in the original search term)
- What additional product features might you add?
  - Additional search term improvements
    - search per "word" separately and see how those match + add weights
    - Misspellings (could use trigram/ngram algorithms), when no results are found
    - Add weight for how "shallow" in the tree you are (assuming shallow is more likely what's being searched)
    - could search on the other data available within each node, though you'd want to visualize those in the UI otherwise it's confusing why they show up in search
    - could do an immediate fast search, then async run more complex fuzzy matching or stretch matches as needed  
    - could keep looking for pre-existing implementations and use those and trust more experienced people in this
  - Add monitoring to searches that end up with zero results to analyze later to understand what people are typing and we fail to find for them.
  - Could keep a list of recent selections and give them additional weight, maybe based on how long ago it was selected
    - Could add a background color to the rows where the strength of the row is how recently they visited an item to draw their eyes to their own history
  - Could add a "star" UI to each item to allow weighing starred items higher
  - Could add "bookmarks" and a specific "jump to bookmark" search
  - I assume these urls will relate to the browser url, so maybe just normal browser bookmarks can work, or urls can be structured to make them bookmarkable
  - Could go hard into predictive analysis... machine learning? ... and recommend where they want to go before they type
  - Could add a shortcut key to pop open this search from anywhere ( ctrl + k in slack )
  
